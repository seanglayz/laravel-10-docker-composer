# Docker-composer tempalte laravel 10
## Project Description
Description of a clean avenue on laravel
## Installation (starting for the first time)
- run in terminall `git clone https://gitlab.com/lutsenkodevmd_php/laravel-10-docker-composer.git .`
- stop all docker `docker kill $(docker ps -q)` and  `docker-compose down`
- run  `docker-compose up --b`
- in docker `docker-compose exec php-fpm_template bash` and run command  `chown -R www-data:www-data storage/`
## Run laravel
- run  `docker-compose up `
